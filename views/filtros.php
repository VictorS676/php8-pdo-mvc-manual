<div class="filters row">
    <div class="col-12">
        <form method="GET">
            <div class="row">
                <?php
                    $i = 0;
                    $filtros = $carros->getFiltros();
                    while(!empty($filtros[$i])){
                        $filtro = $filtros[$i];
                        if($filtro == 'versao'){
                            $filtro = "Versão";
                        }else if($filtro == 'ano_modelo'){
                            $filtro = "Ano/Modelo";
                        }
                        echo '<div class="col-lg-4 col-md-6 col-sm-12">
                                    <label class="label-filters mt-2" for="'.$filtros[$i].'">'.$filtro.'</label>
                                    <select class="form-select" name="'.$filtros[$i].'" id="'.$filtros[$i].'" aria-label="default">
                                        <option selected>---</option>';

                        $j = 0;
                        $opcoes = $carros->getListaFiltros($filtros[$i]);
                        while(!empty($opcoes[$j])){
                            $select = '';
                            if($_REQUEST[$filtros[$i]] == $opcoes[$j]){
                                $select = 'selected';
                            }
                            echo "<option $select value='$opcoes[$j]'>$opcoes[$j]</option>";
                            $j++;

                        }
                                        
                        echo '</select>
                            </div>';
                        $i++;
                    }
                ?>
            </div>

            <div class="row">
                <div class="col-12 text-right">
                    <button type="submit" id="btn-filtro" class="btn btn-primary btn-filter mt-3">Filtrar</button>
                </div>
            </div>
        </form>
    </div>
</div>