    <div class="table-responsive">
        <table class="table table-striped table-hover mt-3">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Marca</th>
                    <th>Modelo</th>
                    <th>Versão</th>
                    <th>Descrição</th>
                    <th>Tipo</th>
                    <th>Categoria</th>
                    <th>Segmento</th>
                    <th>Ano/Fab</th>
                    <th>Ano/Modelo</th>
                    <th>Portas</th>
                    <th>Preço</th>
                    <th>Dta/Cad</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $i = 0;
                    $req = null;
                    if(!empty($_REQUEST)){
                        $req = $_REQUEST;
                    }
                    
                    $veiculo = $carros->getDadosVeiculares($req);
                    if(!empty($veiculo)){
                        foreach($veiculo as $car){
                            echo "<tr>";
                            echo "<td>$car[id]</td>";
                            echo "<td>$car[marca]</td>";
                            echo "<td>$car[modelo]</td>";
                            echo "<td>$car[versao]</td>";
                            echo "<td>$car[descricao]</td>";
                            echo "<td>$car[tipo]</td>";
                            echo "<td>$car[categoria]</td>";
                            echo "<td>$car[segmento]</td>";
                            echo "<td>$car[ano_fabricacao]</td>";
                            echo "<td>$car[ano_modelo]</td>";
                            echo "<td>$car[portas]</td>";
                            echo "<td>R$".$car["preco"]."</td>";
                            echo "<td>$car[dt_cadastro]</td>";
                            echo "</tr>";
                        }
                    }else{
                        echo "<tr>";
                        echo "<td colspan='13'>Não foram encontrados veículos nestas especificações.</td>";
                        echo "</tr>";
                    }
                ?>
            </tbody>
        </table>
    </div>