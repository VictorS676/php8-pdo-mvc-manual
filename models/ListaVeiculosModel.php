<?php

class ListaVeiculosModel{
    private $DB;

    public function __construct(){
        // estabelecer conexão com banco de dados
        $this->DB = new PDO("mysql:host=localhost;dbname=database", "root", "");;
    }

    public function listaFiltros(string $filtro){
        $rs = $this->DB->prepare("SELECT $filtro FROM carros GROUP BY $filtro");
        if($rs->execute()){
            if($rs->rowCount() > 0){
                $i = 0;
                while($row = $rs->fetch(PDO::FETCH_OBJ)){
                    $filtros[$i] = $row->$filtro;
                    $i++;
                }
            }
        }
        return $filtros;
    }

    public function listaVeiculos(array $filtros = null){
        if($filtros != null){
            if(
                $filtros['tipo'] != '---' ||
                $filtros['categoria'] != '---' ||
                $filtros['marca'] != '---' ||
                $filtros['modelo'] != '---' ||
                $filtros['versao'] != '---' ||
                $filtros['ano_modelo'] != '---'
            ){
                $where = "WHERE ";
                $and = 0;
                if($filtros['tipo'] != '---'){
                    $where .= " tipo = '$filtros[tipo]'";
                    $and = 1;
                }
                if($filtros['categoria'] != '---'){
                    if($and == 0){
                        $and = 1;
                    }else{
                        $where .= " AND";
                    }
                    $where .= " categoria = '$filtros[categoria]'";
                }
                if($filtros['marca'] != '---'){
                    if($and == 0){
                        $and = 1;
                    }else{
                        $where .= " AND";
                    }
                    $where .= " marca = '$filtros[marca]'";
                }
                if($filtros['modelo'] != '---'){
                    if($and == 0){
                        $and = 1;
                    }else{
                        $where .= " AND";
                    }
                    $where .= " modelo = '$filtros[modelo]'";
                }
                if($filtros['versao'] != '---'){
                    if($and == 0){
                        $and = 1;
                    }else{
                        $where .= " AND";
                    }
                    $where .= " versao = '$filtros[versao]'";
                }
                if($filtros['ano_modelo'] != '---'){
                    if($and == 0){
                        $and = 1;
                    }else{
                        $where .= " AND";
                    }
                    $where .= " ano_modelo = '$filtros[ano_modelo]'";
                }
                $rs = $this->DB->prepare("SELECT * FROM carros $where");
            }else{
                $rs = $this->DB->prepare("SELECT * FROM carros");
            }
        }else{
            $rs = $this->DB->prepare("SELECT * FROM carros");
        }
        if($rs->execute()){
            $carros = [];
            if($rs->rowCount() > 0){
                $i = 0;
                while($row = $rs->fetch(PDO::FETCH_OBJ)){
                    $carros[$i]['id'] = $row->id;
                    $carros[$i]['marca'] = $row->marca;
                    $carros[$i]['modelo'] = $row->modelo;
                    $carros[$i]['versao'] = $row->versao;
                    $carros[$i]['descricao'] = $row->descricao;
                    $carros[$i]['tipo'] = $row->tipo;
                    $carros[$i]['categoria'] = $row->categoria;
                    $carros[$i]['segmento'] = $row->segmento;
                    $carros[$i]['ano_fabricacao'] = $row->ano_fabricacao;
                    $carros[$i]['ano_modelo'] = $row->ano_modelo;
                    $carros[$i]['portas'] = $row->portas;
                    $carros[$i]['preco'] = $row->preco;
                    $carros[$i]['dt_cadastro'] = $row->dt_cadastro;
                    $i++;
                }
            }
        }
        return $carros;
    }

}

?>