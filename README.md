# CRUD em PHP OO de Automóveis

## Descrição do projeto

O projeto gera uma tabela responsiva com os dados dos automóveis presentes no banco de dados com as seguintes colunas:

- Número Identificador;
- Marca;
- Modelo;
- Versão;
- Descrição;
- Tipo;
- Categoria;
- Segmento;
- Ano de Fabricação;
- Ano do Modelo;
- Portas;
- Preço;
- Data de cadastro.

E disponibiliza filtros de busca nos seguintes campos:
- Tipo;
- Categoria;
- Marca;
- Modelo;
- Versão;
- Ano do Modelo.

---

### Ferramentas

- PHP8;
- MySQL;
- JavaScript ES6:
  - JQuery;
  - Ajax.

### Conceitos
- POO;
- MVC;
- Segurança (validação de dados, tratamento de erros e garantia da integridade dos dados);
- Performance (otimização);
- Documentação.

### Melhorias

1. Usar o PHPRouter para criar um sistema de rotas;
2. Criar uma paginação dinâmica para a listagem;
3. Implemetar métodos de criação, edição e exclusão de registros na tabela 'carros';
4. Otimizar os campos do banco de dados;
5. Uso do Ajax na paginação e na aplicação dos filtros;
6. Utilizar docker (geração do .dockerfile) para conteinirizar o ambiente.