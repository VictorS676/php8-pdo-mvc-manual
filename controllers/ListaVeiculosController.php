<?php

require_once("models/ListaVeiculosModel.php");

class ListaVeiculosController{
    private $model;
    private array $filtros;

    public function __construct(){
        $this->model = new ListaVeiculosModel();
        $this->filtros = ["tipo", "categoria", "marca", "modelo", "versao", "ano_modelo"];
    }

    public function getDadosVeiculares(array $filtros = null){
        if($filtros != null){
            $i = 0;
            while(!empty($filtros[$i])){
                $filtros[$i] = $this->limpaXSSeSQLi($filtros[$i]);
            }
        }
        return $this->model->listaVeiculos($filtros);
    }

    public function getFiltros(){
        return $this->filtros;
    }
    
    public function getListaFiltros(string $filtro){
        return $this->model->listaFiltros($filtro);
    }

    private function limpaXSSeSQLi($str){
        $farr = array("/\\s+/",
            "/<(\\/?)(script|i?frame|style|html|body|title|link|meta|object|\\?|\\%)([^>]*?)>/isU",
            "/(<[^>]*)on[a-zA-Z]+\s*=([^>]*>)/isU",);
        $str = preg_replace($farr,"",$str);
        return addslashes($str);
    }
}

?>